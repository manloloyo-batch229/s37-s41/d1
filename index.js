// npm init -y
// npm i express
// npm i mongoose
// npm i cors -cross origin resource sharing
// npm i bcrypt - for encrypt password
// npm install jsonwebtoken

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const courseRoutes = require("./routes/course");


const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.yy11iud.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

// server for mongoDB
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// MiddleWares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});


