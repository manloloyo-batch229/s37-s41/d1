
const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth")

// Create course | my answer
/*router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true) {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));

	}else{

	res.send("User is not allowed!")
	}

})
*/
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});




// Retrieve all courses
router.get("/all", (req, res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

/*
	Mini-Activity 
	1.Create a route that will retrieve ALL ACTIVE courses (endpoint: "/")
	2.No need for user to login
	3.Create a controller that will return ALL ACTIVE courses

*/
// Retrive all Active courses
// solutin of get all active courses
router.get("/allactive", (req, res) => {
	
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Retrieve a specific course
router.get("/:courseId", (req, res) => {
				//argument(req.params)
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// update a course
// auth.verify kung kailangan naka login yung user
router.put("/:courseId/update", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// archive a specific item
router.put("/archive/:courseId/", auth.verify, (req ,res) => {
	const data = auth.decode(req.headers.authorization);

	if(data.isAdmin == true) {

	courseController.archiveCourse(req.params, req.body).then(resultFromController => { res.send(resultFromController)
	});
	}else{
		res.send("You are not Authorized!")
	}

});

/* Activity answer on archive a specific item
router.put("/:courseId/archive", auth.verify, (req, res) => {
	
	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController));
	
});




*/

// export the router object for index.js file
module.exports = router;

